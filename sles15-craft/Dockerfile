FROM registry.suse.com/bci/bci-base:latest

LABEL Description="KDE Suse Linux Enterprise Server 15 Base for use with Craft"
LABEL maintainer="KDE Sysadmin <sysadmin@kde.org>"

RUN zypper --non-interactive update && \
    zypper --non-interactive install openssh-server \
    # install some core tools
    which git-core cmake gcc12 gcc12-c++ glibc-devel-static xz \
    # python
    python311 python311-pip sqlite3 zlib \
    # localedef
    glibc-i18ndata \
    # qt
    vulkan-devel vulkan-headers wayland-devel Mesa-libGL-devel \
    libX11-devel libX11-xcb1 libxcb-devel xcb-util-keysyms-devel xcb-util-renderutil-devel xcb-util-wm-devel libxkbcommon-x11-devel libxkbcommon-devel libXi-devel xcb-util-image-devel \
    flex bison gperf libicu-devel ruby awk patch \
    alsa-devel libXcomposite-devel libXcursor-devel libXrandr-devel libXtst-devel mozilla-nspr-devel mozilla-nss-devel gperf bison nodejs-default nodejs-devel-default && \
    # cleanup to ensure we don't leave behind anything that doesn't need to be in the image
    zypper --non-interactive clean -a && \
    # make sure the appropriate gcc/python versions are the defaults
    alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 1 && \
    alternatives --install /usr/bin/cc cc /usr/bin/gcc-12 1 && \
    alternatives --install /usr/bin/cpp cpp /usr/bin/g++-12 1 && \
    alternatives --install /usr/bin/g++ gcc++ /usr/bin/g++-12 1 && \
    alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 1 && \
    alternatives --install /usr/bin/pip3 pip3 /usr/bin/pip3-3.11 1

# install powershell
RUN curl -L -o /tmp/powershell.tar.gz https://github.com/PowerShell/PowerShell/releases/download/v7.3.5/powershell-7.3.5-linux-x64.tar.gz && \
    mkdir -p /opt/microsoft/powershell/7 && \
    tar zxf /tmp/powershell.tar.gz -C /opt/microsoft/powershell/7 && \
    rm /tmp/powershell.tar.gz && \
    chmod +x /opt/microsoft/powershell/7/pwsh && \
    ln -s /opt/microsoft/powershell/7/pwsh /usr/bin/pwsh

# install python modules
RUN python3 --version && \
     python3 -m pip install --upgrade pip && \
     python3 -m pip install pyyaml lxml paramiko

# Setup a user account for everything else to be done under
RUN useradd -d /home/appimage/ -u 1000 --user-group --create-home -G video appimage

# Get locales in order
RUN localedef -c -i en_US -f UTF-8 en_US.UTF-8

# let git work
RUN git config --system merge.defaultToUpstream true

# required by qtbase and not available in the repository
RUN curl -OL https://xcb.freedesktop.org/dist/xcb-util-cursor-0.1.4.tar.xz && \
    tar xf xcb-util-cursor-0.1.4.tar.xz && \
    cd xcb-util-cursor-0.1.4 && \
    ./configure --prefix /usr/ --enable-static --disable-shared CFLAGS=-fPIC && \
    make -j5 && \
    make install && \
    cd .. && \
    rm -Rf xcb-util-cursor-0.1.4

# Finally drop to an unprivileged user account
USER appimage
